<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Conversation;
use App\Models\Message;
use App\Models\Participant;
use App\Models\Friendship;
use App\Models\Notification;
class DatabaseSeeder extends Seeder
{
    public function run()
    {
        User::factory()->count(100)->create();
        Conversation::factory()->count(50)->create();
        Message::factory()->count(200)->create();
        Participant::factory()->count(100)->create();
        Friendship::factory()->count(100)->create();
        Notification::factory()->count(100)->create();
    }
}
