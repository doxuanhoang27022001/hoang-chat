<?php

use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::group(['middleware' => ['auth:sanctum']], function () {
    //
    Route::post('/auth/logout', [AuthController::class, 'logoutUser']);
    Route::prefix('user')->group(function () {
        Route::get('/get',[UserController::class,'getUser'])->name('get.user');
        Route::put('/update',[UserController::class,'update'])->name('update.user');
    });
    Route::prefix('conversation')->group(function () {
        Route::get('/get',[UserController::class,'getConversation'])->name('get.conversation');
    });
});

Route::post('/auth/register', [AuthController::class, 'createUser']);
Route::post('/auth/login', [AuthController::class, 'loginUser']);
