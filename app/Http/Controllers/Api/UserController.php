<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use http\Env\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    //
    public function getUsers()
    {
        $users = User::all();
        return response()->json($users);
    }
    public function update(Request $request)
    {
        $user = Auth::user();
    if($user){
        try {
            $userData = [
                'name' => $request->input('name'),
                'email' => $request->input('email'),
                'phone' => $request->input('phone'),
                'address' => $request->input('address'),
                'avatar' => $request->input('avatar'),
                'date_of_birth' => $request->input('date_of_birth'),
                'password' => $request->input('password')
            ];

            // Loại bỏ các giá trị null khỏi mảng dữ liệu người dùng
            $userData = array_filter($userData, function ($value) {
                return $value !== null;
            });

            // Cập nhật thông tin người dùng
            $user->update($userData);

            return response()->json([
                'status' => true,
                'message' => 'Đã cập nhật thành công'
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'status' => false,
                'message' => 'Cập nhật thất bại',
                'error' => $e->getMessage()
            ], 500);
        }
    }

    }
    public function getUser(){
        $user = Auth::user();
        if ($user) {
            return response()->json($user, 200);
        } else {
            return response()->json([
                'message' => 'Unauthorized'
            ], 401);
        }
    }

}
